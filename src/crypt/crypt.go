package crypt

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type Ticker struct {
	Base   string `json:"base"`   // Base currency code
	Target string `json:"target"` // Target currency code
	Price  string `json:"price"`  // Volume-weighted price
	Volume string `json:"volum"`  // Total trade volume for the last 24 hours
	Change string `json:"change"` // Past hour price change
}

type TiсkerResponse struct {
	Timestamp int64  `json:"timestamp"` // Update time in Unix timestamp format
	Success   bool   `json:"success"`   // True or false
	Error     string `json:"error"`     // Error description
	Ticker    Ticker `json:"ticker"`
}

func GetCryptoCurrency(url string, target string) string {

	c := http.Client{}
	resp, err := c.Get(url + target + "-usd")
	if err != nil {
		return "cryptonator API not responding"
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	ticker := TiсkerResponse{}

	err = json.Unmarshal(body, &ticker)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(ticker.Ticker)
	result, _ := strconv.ParseFloat(ticker.Ticker.Price, 64)
	return "1 " + target + " = " + fmt.Sprintf("%.2f", result) + "$"
}
