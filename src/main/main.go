package main

import (
	"crypt"
	"joke"
	"log"
	"net/http"
	"os"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

// для вендоринга используется GB
// сборка проекта осуществляется с помощью gb build
// установка зависимостей - gb vendor fetch gopkg.in/telegram-bot-api.v4
// установка зависимостей из манифеста - gb vendor restore

var buttons = []tgbotapi.KeyboardButton{
	{Text: "Анекдот 18+"},
	{Text: "Биток"},
	{Text: "Рипл"},
	{Text: "Эфир"},
	{Text: "DMarket"},
}

// При старте приложения, оно скажет телеграму ходить с обновлениями по этому URL
const WebhookURL = "https://msu-go-2019.herokuapp.com/"
const JOKE_URL = "http://rzhunemogu.ru/Rand.aspx?CType=11"
const CRYPT_URL = "https://api.cryptonator.com/api/ticker/"
const BOT_API_KEY = "717242944:AAFfUb9dcEj68ysLLb0CSSF05oFs4j-OxBw"

func main() {
	// Heroku прокидывает порт для приложения в переменную окружения PORT
	port := os.Getenv("PORT")
	bot, err := tgbotapi.NewBotAPI(BOT_API_KEY)
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// Устанавливаем вебхук
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		log.Fatal(err)
	}

	updates := bot.ListenForWebhook("/")
	go http.ListenAndServe(":"+port, nil)

	// получаем все обновления из канала updates
	for update := range updates {
		var message tgbotapi.MessageConfig
		log.Println("received text: ", update.Message.Text)

		switch update.Message.Text {
		case "Анекдот 18+":
			message = tgbotapi.NewMessage(update.Message.Chat.ID, joke.GetJokeRU(JOKE_URL))
		case "Биток":
			message = tgbotapi.NewMessage(update.Message.Chat.ID, crypt.GetCryptoCurrency(CRYPT_URL, "BTC"))
		case "Рипл":
			message = tgbotapi.NewMessage(update.Message.Chat.ID, crypt.GetCryptoCurrency(CRYPT_URL, "XRP"))
		case "Эфир":
			message = tgbotapi.NewMessage(update.Message.Chat.ID, crypt.GetCryptoCurrency(CRYPT_URL, "ETH"))
		case "DMarket":
			message = tgbotapi.NewMessage(update.Message.Chat.ID, crypt.GetCryptoCurrency(CRYPT_URL, "DMT"))
		default:
			message = tgbotapi.NewMessage(update.Message.Chat.ID, `Press "Анекдот 18+" to receive joke`)
		}

		// В ответном сообщении просим показать клавиатуру
		message.ReplyMarkup = tgbotapi.NewReplyKeyboard(buttons)

		bot.Send(message)
	}
}
