package joke

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
)

type JokeResponseRU struct {
	Content string `xml:"content"`
}

func GetJokeRU(url string) string {
	c := http.Client{}
	resp, err := c.Get(url)
	if err != nil {
		return "jokes API not responding"
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	joke := JokeResponseRU{}

	body = ConvertCp1251ToUTF8(body)
	err = xml.Unmarshal(body, &joke)
	if err != nil {
		return "Joke error"
	}

	return joke.Content
}
