package joke

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetJokeRU(t *testing.T) {
	resp := `<root><content>The best joke</content></root>`

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(resp))
	}))

	joke := GetJokeRU(ts.URL)

	t.Log("Joke: ", joke)

	if joke != "The best joke" {
		t.Error("Joke is not the best :(")
	}

	resp = "Bad joke"

	joke = GetJokeRU(ts.URL)

	if joke != "Joke error" {
		t.Error("Joke must be bad")
	}
}
